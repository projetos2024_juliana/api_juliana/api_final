const express = require('express');
const app = require('./index');
const reservasRoutes = require('./controller/reservasController'); 

app.use(express.json());


app.use('/api', reservasRoutes); 

// Iniciar o servidor
app.listen(5000, () => {
    
    console.log('Servidor conectado na porta 5000.');
})

