const router = require('express').Router()
const userController = require('../controller/userController')
const signInController = require('../controller/signInController')
const guiaController = require('../controller/guiaContoller')
const dbController = require('../controller/dbController')
const reservasController = require('../controller/reservasController')

// rotas para user
router.post('/user/', userController.postUser)
router.put('/editaruser/:id', userController.editarUser)
router.delete('/user/:id', userController.deleteUser)
router.post('/sign-in/', signInController.postSignIn)
router.get('/user/', userController.getUser)
router.post('/login', userController.logUser)

// rotas para o guia 
router.post('/guia/', guiaController.postGuia)
router.put('/editarguia/', guiaController.updateGuia)
router.delete('/guia/:id', guiaController.deleteGuia)
router.get('/guiag/', guiaController.getGuia)

// rota para vizualização dos nomes e descrições das tabelas.
router.get("/tables", dbController.getTables)  // rota para exibir os nomes das tabelas.
router.get("/tablesDescription", dbController.getTablesDescriptions) // rota para exibir as descrições das tabelas.


// Rota para criar uma nova reserva
router.post('/reservas', reservasController.postReserva);

// Rota para atualizar uma reserva existente
router.put('/reservas/:id', reservasController.updateReserva);

// Rota para deletar uma reserva
router.delete('/reservas/:id/', reservasController.deleteReserva);

// Rota para obter todas as reservas de um guia específico
router.get('/reservas/:id', reservasController.getReservas);

module.exports = router;