class userController {
  static async postUser(req, res) {
    const { nome, senha, email, telefone } = req.body;
    // Validar email
    if (!email.includes("@")) {
      return res.status(400).json({ message: "O email deve conter '@'." });
    }

    // Validar senha para ter no mínimo 4 caracteres
    if (senha.length >= 4) {
      return res
        .status(400)
        .json({ message: "A senha deve ter no mínimo 4 caracteres." });
    }

    // Verifica se o nome do guia começa com uma letra maiúscula
        if (!/^[A-ZÀ-Ú]/.test(nome)) {
          return res.status(400).json({ message: "O nome do usuário deve começar com letra maiúscula." });
      }

    if (nome === "" || senha === "" || email === "" || telefone === "") {
      res.status(400).json({ message: "Campos inválidos." });
    } else {
      res.status(200).json({ message: "Nome cadastrado com sucesso." });
    }
  }

  static async editarUser(req, res) {
    const { nome, senha, email, telefone } = req.body;
    if (!email.includes("@")) {
      return res.status(400).json({ message: "O email deve conter '@'." });
    }

    // Verifica se o nome do guia começa com uma letra maiúscula
    if (!/^[A-ZÀ-Ú]/.test(nome)) {
      return res.status(400).json({ message: "O nome do guia deve começar com letra maiúscula." });
    }

    // Validar senha para ter no mínimo 4 caracteres
    if (senha.length >= 4) {
      return res
        .status(400)
        .json({ message: "A senha deve ter no mínimo 4 caracteres." });
    }
    if ((nome !== "" && senha !== "" && email !== "" && telefone === "")) {
      console.log(" O nome foi editado com os seguintes dados: ", req.body);
      res.status(200).json({ message: "O nome foi atualizado" });
    } else {
      res.status(400).json({ message: "  Dados inválidos " });
    }
  }

  static async deleteUser(req, res) {
    console.log(req.params.id);
    if (req.params.id !== "") {
      console.log(" O nome foi deletado ", req.body);
      res.status(200).json({ message: "  O nome foi deletado " });
    } else {
      res.status(400).json({ message: "  O nome não foi encontrado " });
    }
  }

  //Listar
  static async getUser(req, res) {
    res.status(200).json({
      algumaMensagem: {
        algumaMensagem: "Reserva de guia",
        algumNúmero: 26,
      },
    });
  }

  static async logUser(req, res){
    const { email, senha } = req.body

    if( email === "thayne@gmail.com" && senha === "12345")
    {
      res.status(200).json({ message: "Login efetuado com sucesso." });
    }
    else{
      res.status(401).json({ message: "Falha no login." });
    }

  }

};

module.exports = userController
