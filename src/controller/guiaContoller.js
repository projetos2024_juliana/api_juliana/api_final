class guiaController {
    static async postGuia(req, res) {
        const { nome, email, telefone, idioma, valor, complemento, disponibilidade,} = req.body;
        
        // Verifica se o nome do guia começa com uma letra maiúscula
        if (!/^[A-ZÀ-Ú]/.test(nome)) {
          return res.status(400).json({ message: "O nome do guia deve começar com letra maiúscula." });
        }
        
        // Validação do email do guia (se há @ no email)
        if (!email || !email.includes("@")) {
            return res.status(400).json({ message: "O email deve conter '@'." });
        }
        

        // Validar se há campo em branco
        if (nome === "" || email === "" || idioma === "" || valor === "" || telefone === "" || complemento === "") {
            return res.status(400).json({ message: "Campos inválidos." });
        } else {
            return res.status(200).json({ message: "Cadastrado com sucesso." });
        }
    }

    static async updateGuia(req, res) {
        const { nome, email, idioma, valor, telefone, complemento, disponibilidade} = req.body;
        
        // Validação do email do guia (se há @ no email)
        if (!email || !email.includes("@")) {
            return res.status(400).json({ message: "O email deve conter '@'." });
        }

        // Verifica se o nome do guia começa com uma letra maiúscula
        
        if (!/^[A-ZÀ-Ú]/.test(nome)) {
            return res.status(400).json({ message: "O nome do guia deve começar com letra maiúscula." });
        }

        //analisando se não há campo em branco para depois atualizar
        if (nome !== "" && email !== "" && idioma !== "" && valor !== 0 && telefone !== "" && complemento !== "") {
            console.log("O guia foi editado");
            return res.status(200).json({ message: "O guia foi atualizado" });
        } else {
            return res.status(400).json({ message: "Dados inválidos" });
        }
    }

    static async deleteGuia(req, res) {
        //ver se o id foi fornecido na URL para depois deletar
        console.log(req.params.id);
        if (!req.params.id) {
            return res.status(400).json({ message: "O ID do guia não foi fornecido." });
        }

        console.log("O guia foi deletado", req.body);
        return res.status(200).json({ message: "O guia foi deletado" });
    }

    static async getGuia(req, res) {
        //guias fornecidos de forma ficticía para visualização no post 
        const guiaCadastrados = [
          {
            nome: "Mariana",
            email: "mariana87@hotmail.com",
            idioma: "português/espanhol",
            valor: 180.0,
            telefone: "5541987654321",
            complemento: "Experiência de 15 anos nesse ramo.",
          },
          {
            nome: "Lucas",
            email: "lucas.traveler@gmail.com",
            idioma: "inglês/francês",
            valor: 200.0,
            telefone: "447712345678",
            complemento:
              "Amante de viagens e fotografia, pronto para compartilhar dicas e experiências únicas.",
          },
          {
            nome: "Sofia",
            email: "sofia.adventures@gmail.com",
            idioma: "português/italiano",
            valor: 120.0,
            telefone: "5511998765432",
            complemento:
              "Guia turística apaixonada por história e cultura, pronta para tornar sua viagem inesquecível.",
          },
          {
            nome: "Pedro",
            email: "pedro.explore@yahoo.com",
            idioma: "português/inglês",
            valor: 160.0,
            telefone: "5561987654321",
            complemento:
              "Aventureiro e entusiasta de esportes radicais, oferecendo roteiros emocionantes para os amantes da adrenalina.",
          },
          {
            nome: "Isabela",
            email: "isa.travelguide@gmail.com",
            idioma: "inglês/espanhol",
            valor: 150.0,
            telefone: "523376543210",
            complemento:
              "Viajante experiente e historiadora amadora, pronta para compartilhar curiosidades e histórias fascinantes sobre cada destino.",
          },
        ];
    
        
        return res.status(200).json({
          mensagem: { guiaCadastrados },
        });
      }
};

module.exports = guiaController
