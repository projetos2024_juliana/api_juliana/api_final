const connect = require("../db/connect")

module.exports = class dbController{
    static async getTables(req, res) {
        // Consulta para obter a lista de tabelas
        const queryShowTables = "SHOW TABLES";
    
        connect.query(queryShowTables, function(err, result, fields) {
            // Verifica que ocorreu algum erro na consulta
            if (err) {
                console.log('Erro ao obter as tabelas:', err);
                return res.status(500).json({ error: "Erro ao obter tabelas do banco de dados" });
            }
    
            // Extrai os nomes das tabelas dos resultados da consulta e os envia como resposta
            const tableNames = result.map(row => row[Object.keys(row)[0]]);
            res.status(200).json({ tables: tableNames });
        });
    }

    static async getTablesDescriptions(req, res) {
        // Consulta para obter a descrição de todas as tabelas e seus atributos
        const queryDescTables = "SHOW TABLES";
    
        // Faz a execução da consulta SQL
        connect.query(queryDescTables, async function(err, result, fields) {
            // Verifica se ocorreu algum erro na consulta 
            if (err) {
                console.log('Erro ao obter as tabelas:', err);
                return res.status(500).json({ error: "Erro ao obter tabelas do banco de dados" });
            }
    
            const tables = [];  // Array para armazenar as descrições das tabelas
    
            for (let i = 0; i < result.length; i++) {
                const tableName = result[i][Object.keys(result[i])[0]];  // Obtém o nome da tabela 
                const queryDescTable = `DESCRIBE ${tableName}`;  // Consulta para obter a descrição das tabelas         
    
                try {
                    // Executa a consulta SQL de descrição da tabela
                    const tableDescription = await new Promise((resolve, reject) => {
                        connect.query(queryDescTable, function(err, result, fields) {
                            if (err) {
                                reject(err);  // Rejeita ( se ocorrer um erro)
                            }
                            resolve(result);  // Resolve  ( com os resultados da consulta)
                        });
                    });

                    // Adiciona o nome e a descrição da tabela ao array de tabelas
                    tables.push({ name: tableName, description: tableDescription });
                } catch (error) {
                    // Trata erros que ocorra durante a obtenção da descrição da tabela
                    console.log(error);
                    return res.status(500).json({ error: "Erro ao obter a descrição da tabela!" });
                }
            }
    
            res.status(200).json({ tables });  // Resposta com as descrições das tabelas
        });
    }
    
}