class reservasController {

    static async postReserva(req, res) {
        const { nome, periodo, valor, data_de_inicio, data_de_fim, valor_total } = req.body;

        // validar se há algum campo em branco 
        if ( nome === "" || periodo === "" ||  valor === "" || data_de_inicio === "" || data_de_fim === "" || valor_total === "" ) {
            return res.status(400).json({ message: "Todos os campos devem estar preenchidos corretamente!" });
        } else {
            return res.status(200).json({ message: "Reserva realizada com sucesso!" });
        };
    };

    static async updateReserva (req, res) {
        const { nome, periodo, valor, data_de_inicio, data_de_fim, valor_total } = req.body;

        // analisando se não há campo em branco para depois atualizar a reserva
        if ( nome === "" || periodo === "" ||  valor === "" || data_de_inicio === "" || data_de_fim === "" || valor_total === "" ) {
            console.log("A reserva foi editada com sucesso!");
            return res.status(200).json({ message: "Reserva atualizada!" });
        } else {
            return res.status(400).json({ message: "Os dados estão inválidos" });
        }
    };

    static async deleteReserva(req, res) {

        // ver se o id foi fornecido na URL para depois deletar
        console.log(req.params.id);
        if (!req.params.id) {
            return res.status(400).json({ message: "O ID da reserva não foi fornecido." });
        }

        console.log("A reserva foi deletada!", req.body);
        return res.status(200).json({ message: "A reserva foi deletada!" });
    };

    
    static async getReservas(req, res) {
        // lógica para obter as informações das reservas
        try {
            // informações de reservas a partir do banco de dados
            const guiaId = req.params.id;
            if (!guiaId) {
                return res.status(400).json({ message: "O ID do guia não foi fornecido." });
            }
            
            const reservas = [
                { id: 1, nome: "Reserva 1", periodo: "10 dias", valor: 1000, data_de_inicio: "2023-05-01", data_de_fim: "2023-05-10", valor_total: 1000 },
                { id: 2, nome: "Reserva 2", periodo: "5 dias", valor: 500, data_de_inicio: "2023-06-01", data_de_fim: "2023-06-06", valor_total: 500 }
            ];

            return res.status(200).json(reservas);
        } catch (error) {
            return res.status(500).json({ message: "Erro ao buscar as informações das reservas." });
        }
    }
}

module.exports = reservasController;







