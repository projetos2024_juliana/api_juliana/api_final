const express = require("express");
const app = express();
const cors = require("cors");
const testConnect = require("./db/testConnect")


class AppController {
  constructor() {
    this.express = express();
    this.middlewares();
    this.routes();
    testConnect();

  }
  middlewares() {
    this.express.use(express.json());
    this.express.use(cors())
  }
  routes() {
    const apiRoutes = require('./routes/routes');
    this.express.use("/api", apiRoutes);
    this.express.get("/testeApi/", (_, res) => {
      res.send({ status: "Api está ligada." });
    });
  }
}
module.exports = new AppController().express;
